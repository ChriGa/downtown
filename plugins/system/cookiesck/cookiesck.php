<?php

/**
 * @copyright	Copyright (C) 2015 Cédric KEIFLIN alias ced1870
 * http://www.joomlack.fr
 * @license		GNU/GPL
 * */
 
 /**
 * Cookies management Javascript code from
 * @subpackage		Modules - mod_jbcookies
 * 
 * @author			JoomBall! Project
 * @link			http://www.joomball.com
 * @copyright		Copyright © 2011-2014 JoomBall! Project. All Rights Reserved.
 * @license			GNU/GPL, http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.plugin.plugin');

class plgSystemCookiesck extends JPlugin {

	private $paramsEnabled;

	function __construct(&$subject, $config) {
		$this->paramsEnabled = file_exists(JPATH_SITE . '/administrator/components/com_cookiesck/cookiesck.php');
		parent :: __construct($subject, $config);
	}

	function onAfterDispatch() {
		global $ckjqueryisloaded;
		$app = JFactory::getApplication();
		$document = JFactory::getDocument();
		$doctype = $document->getType();

		// si pas en frontend, on sort
		if ($app->isAdmin()) {
			return false;
		}

		// si pas HTML, on sort
		if ($doctype !== 'html') {
			return;
		}

		// si en mode imbriqué
		if ($app->input->get('tmpl','') != '') {
			return;
		}

		// load jquery
		$jquerycall = "";
		if (version_compare(JVERSION, '3') >= 1 ) { 
			JHTML::_('jquery.framework', true);
		} else if (! $ckjqueryisloaded) {
			$document->addScript(JUri::base(true) . "/plugins/system/cookiesck/assets/jquery.min.js");
		}

		// get the params from the plugin options
		$plugin = JPluginHelper::getPlugin('system', 'cookiesck');
		$pluginParams = new JRegistry($plugin->params);

		// load the language strings of the plugin
		$this->loadLanguage();

		$lifetime = (int) $pluginParams->get('lifetime', 365);
		// set the cookie from the ajax request on click
		if(isset($_POST['set_cookie'])):
				if($_POST['set_cookie']==1)
					setcookie("cookiesck", "yes", time()+3600*24*$lifetime, "/");
		endif;

		$readmore_link = '';
		$link_rel = '';
		if ($pluginParams->get('linktype', 'article') == 'article') {
			$id = $pluginParams->get('article_readmore');
			// $model->setState('filter.language', $app->getLanguageFilter());
			if ($id) {
				require_once JPATH_SITE.'/components/com_content/helpers/route.php';
				require_once JPATH_SITE.'/components/com_content/helpers/association.php';
				JModelLegacy::addIncludePath(JPATH_SITE.'/components/com_content/models', 'ContentModel');
				$langTag = $app->getLanguage()->getTag();

				$assoc_articles = ContentHelperAssociation ::getAssociations($id);
				if (isset ($assoc_articles[$langTag])) {
					$readmore_link = JRoute::_($assoc_articles[$langTag]);
				} else {
					// Get an instance of the generic article model
					$model = JModelLegacy::getInstance('Article', 'ContentModel', array('ignore_request' => true));
					// Set application parameters in model
					$appParams = JFactory::getApplication()->getParams();
					$model->setState('params', $appParams);
					//	Retrieve Content
					$item = $model->getItem($pluginParams->get('article_readmore'));
					$item->slug = $item->id.':'.$item->alias;
					// $item->catslug = $item->catid.':'.$item->category_alias;
					// get the article link
					$readmore_link = JRoute::_(ContentHelperRoute::getArticleRoute($item->slug, $item->catid));
				}
				if ($link_anchor = $pluginParams->get('link_anchor')) {
					$readmore_link = $readmore_link . '#' . trim($link_anchor, '#');
				}
				$link_rel = $pluginParams->get('link_rel') ? ' rel=\"' . $pluginParams->get('link_rel') . '\"' : '';
			}
		} else if($pluginParams->get('linktype', 'article') == 'menuitem') {
			$readmore_link = $pluginParams->get('menuitem_readmore');
			$associations = MenusHelper::getAssociations($readmore_link);
			$langTag = $app->getLanguage()->getTag();
			$link_id = isset($associations[$langTag]) ? $associations[$langTag] : $readmore_link;

			// search for the link
			$db = JFactory::getDbo();
			$query = "SELECT link FROM #__menu WHERE id = " . (int)$link_id;
			$db->setQuery($query);
			$menuItem = $db->loadObject();

			$readmore_link = JRoute::_($menuItem->link);
		} else {
			$readmore_link = $pluginParams->get('link_readmore');
			if (substr($readmore_link, 0,4) != 'http') {
				$readmore_link = JUri::root(true) . '/' . trim($readmore_link, '/');
			}
		}

		$where = 'top';
		switch ($pluginParams->get('position', 'absolute')) {
			case 'absolute':
			default:
				$position = 'absolute';
				break;
			case 'fixed':
				$position = 'fixed';
				break;
			case 'relative':
				$position = 'relative';
				break;
			case 'bottom':
				$position = 'fixed';
				$where = 'bottom';
				break;
		}
		// add styling
		$css = "
			#cookiesck {
				position:" . $position . ";
				left:0;
				right: 0;
				" . $where . ": 0;
				z-index: 99;
				min-height: 30px;
				color: " . $pluginParams->get('text_color', '#eee') . ";
				background: " . $this->hex2RGB($pluginParams->get('background_color', '#000000'), $pluginParams->get('background_opacity', '0.5')) . ";
				box-shadow: #000 0 0 2px;
				text-align: center;
				font-size: 14px;
				line-height: 14px;
			}
			#cookiesck_text {
				padding: 10px 0;
				display: inline-block;
			}
			#cookiesck_buttons {
				float: right;
			}
			#cookiesck_readmore {
				float:right;
				padding:10px;
				border-radius: 3px;
			}
			#cookiesck_accept{
				float:left;
				padding:10px;
				margin: 1px;
				border-radius: 3px;
				background: #000;
				cursor: pointer;
				-webkit-transition: all 0.2s;
				transition: all 0.2s;
				border: 1px solid #404040;
			}
			#cookiesck_accept:hover{
				font-size: 120%;
			}
		";

		$layout = 'layout1';
		if (! $this->paramsEnabled) {
			$document->addStyleDeclaration($css);
		} else {
			$styles = $this->getStylesCss(1);
			// if no style saved in the interface, then still use the default styles
			if (! $styles->layoutcss) {
				$document->addStyleDeclaration($css);
			} else {
				$this->loadAssets($styles, $where, $position);
				$stylesParams = json_decode($styles->params);
				$layout = isset($stylesParams->barlayout) ? $stylesParams->barlayout : 'layout1';
			}
		}

		// create the JS to manage the action
		$js = 'jQuery(document).ready(function($){
				$("#cookiesck").remove();
				$("body").append("<div id=\"cookiesck\" data-layout=\"' . $layout . '\"/>");
				$("body").append("<div id=\"cookiesck_overlay\" />");
				
				$("#cookiesck").append("<span class=\"cookiesck_inner\">'.JText::_('COOKIESCK_INFO').' Weitere Informationen erhalten Sie in unserer </span>")
					' . ($readmore_link ? '.append("<a href=\"' . $readmore_link . '\" ' . $link_rel . ' target=\"' . ($pluginParams->get('link_target', 'same') == 'new' ? '_blank' : '') . '\" id=\"cookiesck_readmore\">'.JText::_('COOKIESCK_MORE').'</a> und <a class=\"cookieImp\" href=\"/impressum\"> Impressum</a>")' : '')  . '
					.append("<div id=\"cookiesck_accept\">'.JText::_('COOKIESCK_ACCEPT').'</div>")
					.append("<div style=\"clear:both;\"></div>");
		
			function ckSetCookie(c_name,value,exdays) {
				var exdate=new Date();
				exdate.setDate(exdate.getDate() + exdays);
				var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString()) + "; path=/";
				document.cookie=c_name + "=" + c_value;
			}

			function ckReadCookie(name) {
				var nameEQ = name + "=";
				var cooks = document.cookie.split(\';\');
				for(var i=0;i < cooks.length;i++) {
					var c = cooks[i];
					while (c.charAt(0)==\' \') c = c.substring(1,c.length);
						if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
					}
				return null;
			}

			var $cookieck = jQuery(\'#cookiesck\');
			var $cookiesck_accept = jQuery(\'#cookiesck_accept\');
			var cookiesck = ckReadCookie(\'cookiesck\');
			$cookieck.hide();
			if(!(cookiesck == "yes")){
				// $cookieck.delay(1000).slideDown(\'fast\'); 
				$cookieck.show(); 

				$cookiesck_accept.click(function(){
					ckSetCookie("cookiesck","yes",'.$lifetime.');
					jQuery.post(\''.JUri::current().'\', \'set_cookie=1\', function(){});
					$cookieck.slideUp(\'slow\');
				});
			} 
		});
		';
		$document->addScriptDeclaration($js);
	}
	
	/**
	 * Convert a hexa decimal color code to its RGB equivalent
	 *
	 * @param string $hexStr (hexadecimal color value)
	 * @param boolean $returnAsString (if set true, returns the value separated by the separator character. Otherwise returns associative array)
	 * @param string $seperator (to separate RGB values. Applicable only if second parameter is true.)
	 * @return array or string (depending on second parameter. Returns False if invalid hex color value)
	 */
	function hex2RGB($hexStr, $opacity) {
		if ($opacity > 1) $opacity = $opacity/100;
		$hexStr = preg_replace("/[^0-9A-Fa-f]/", '', $hexStr); // Gets a proper hex string
		$rgbArray = array();
		if (strlen($hexStr) == 6) { //If a proper hex code, convert using bitwise operation. No overhead... faster
			$colorVal = hexdec($hexStr);
			$rgbArray['red'] = 0xFF & ($colorVal >> 0x10);
			$rgbArray['green'] = 0xFF & ($colorVal >> 0x8);
			$rgbArray['blue'] = 0xFF & $colorVal;
		} elseif (strlen($hexStr) == 3) { //if shorthand notation, need some string manipulations
			$rgbArray['red'] = hexdec(str_repeat(substr($hexStr, 0, 1), 2));
			$rgbArray['green'] = hexdec(str_repeat(substr($hexStr, 1, 1), 2));
			$rgbArray['blue'] = hexdec(str_repeat(substr($hexStr, 2, 1), 2));
		} else {
			return false; //Invalid hex color code
		}
		$rgbacolor = "rgba(" . $rgbArray['red'] . "," . $rgbArray['green'] . "," . $rgbArray['blue'] . "," . $opacity . ")";

		return $rgbacolor;
	}

	/**
	 * Load the scripts and styles
	 */
	protected function loadAssets($styles, $where, $position) {
		if (! $this->paramsEnabled) return;

		// loads the helper in any case
		require_once JPATH_SITE . '/administrator/components/com_cookiesck/helpers/helper.php';
		$doc = JFactory::getDocument();

		$stylescss = $styles->layoutcss;
		$cssreplacements = CookiesckHelper::getCssReplacement();
		global $ckcustomgooglefontslist;
		foreach ($cssreplacements as $tag => $rep) {
			$stylescss = str_replace($tag, $rep, $stylescss);

			$stylesParams = json_decode($styles->params);
//			$layout = isset($stylesParams->barlayout) ? $stylesParams->barlayout : 'layout1';
			$search = array('[', ']');
			$replace = array('', '');
			$var = str_replace($search, $replace, $tag);

			if (isset($stylesParams->{$var . 'textisgfont'}) && $stylesParams->{$var . 'textisgfont'} == '1' 
				&& isset($stylesParams->{$var . 'textgfont'}) && $stylesParams->{$var . 'textgfont'} != '') {
				$ckcustomgooglefontslist[] = $stylesParams->{$var . 'textgfont'};
				
			}
		}
		

		// $styles = str_replace('|ID|', '.customfieldsck.' . $fieldClass, $styles);
		$stylescss = str_replace('#cookiesck_overlay {', '#cookiesck_overlay { 
position: fixed;
display: block;
content: \"\";
top: 0;
bottom: 0;
left: 0;
right: 0;
z-index: 1000;
background-size: cover !important;', $stylescss);
		$stylescss = str_replace('|ID|', '', $stylescss);

		$stylescss .= "
#cookiesck {
	position:" . $position . ";
	left:0;
	right: 0;
	" . $where . ": 0;
	z-index: 1001;
	min-height: 30px;
	box-sizing: border-box;
}
#cookiesck_text {
	display: inline-block;
}
.cookiesck_button {
	display: inline-block;
	cursor: pointer;
}
#cookiesck > .inner {
	display: block;
	flex: 1 1 auto;
	text-align: center;
}
#cookiesck[data-layout=\"layout1\"] #cookiesck_buttons {
	float: right;
}
#cookiesck[data-layout=\"layout2\"] #cookiesck_text,
#cookiesck[data-layout=\"layout2\"] #cookiesck_buttons, 
#cookiesck[data-layout=\"layout3\"] #cookiesck_text,
#cookiesck[data-layout=\"layout3\"] #cookiesck_buttons {
	display: block;
}
#cookiesck[data-layout=\"layout3\"] {
	bottom: 0;
	display: flex;
	align-items: center;
	margin: auto;
	position: fixed;
}
		";

		$doc->addStyleDeclaration($stylescss);
	}

	/**
	 * Check if we need to load the styles in the page
	 */
	public function onBeforeRender() {
//		if (! count($this->styles)) return;
//		$styles = implode("\n", $this->styles);
		$doc = JFactory::getDocument();
		// css
//		$doc->addStyleDeclaration($styles);
		// js
//		JHtml::_('jquery.framework');

		$this->loadCustomGoogleFontsList();
	}

	/**
	 * Load the fonts only if not already registered by another extension
	 */
	public function loadCustomGoogleFontsList() {
		global $ckcustomgooglefontslist;

		if (! empty($ckcustomgooglefontslist)) {
			$doc = JFactory::getDocument();
			foreach ($ckcustomgooglefontslist as $ckcustomgooglefont) {
				$ckcustomgooglefont = str_replace(' ', '+', $ckcustomgooglefont);
				$doc->addStylesheet('//fonts.googleapis.com/css?family=' . $ckcustomgooglefont);
			}
		}
	}

	/**
	 * Get the css rules from the styles
	 *
	 * @param int $id
	 * @return string
	 */
	protected function getStylesCss($id) {
		$db = JFactory::getDbo();
		$q = "SELECT params,layoutcss,state from #__cookiesck_styles WHERE id = " . (int)$id;
		$db->setQuery($q);
		$styles = $db->loadObject();

		return $styles;
	}
}