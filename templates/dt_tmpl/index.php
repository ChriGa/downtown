<?php
/**
 * @package                Joomla.Site
 * @subpackage        Templates.beez5
 * @copyright        Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license                GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

// check modules
$showRightColumn        = ($this->countModules('position-3') or $this->countModules('position-6') or $this->countModules('position-8'));
$showbottom                        = ($this->countModules('position-9') or $this->countModules('position-10') or $this->countModules('position-11'));
$showleft                        = ($this->countModules('position-4') or $this->countModules('position-7') or $this->countModules('position-5'));

if ($showRightColumn==0 and $showleft==0) {
        $showno = 0;
}

JHtml::_('behavior.framework', true);

// get params
$color                        = $this->params->get('templatecolor');
$logo                        = $this->params->get('logo');
$navposition        = $this->params->get('navposition');
$app                        = JFactory::getApplication();
$doc                        = JFactory::getDocument();
$templateparams        = $app->getTemplate(true)->params;

$app = JFactory::getApplication();
$menu = $app->getMenu();
$active = $menu->getActive();
$itemId = $active->id;

//$doc->addScript($this->baseurl.'/templates/'.$this->template.'/javascript/md_stylechanger.js', 'text/javascript', true);
?>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>" >
        <head>
                <jdoc:include type="head" />
                <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
                <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/position.css" type="text/css" media="screen,projection" />
                <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/layout.css" type="text/css" media="screen,projection" />
                <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/print.css" type="text/css" media="Print" />
                <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/beez5.css" type="text/css" />
<?php
        $files = JHtml::_('stylesheet', 'templates/'.$this->template.'/css/general.css', null, false, true);
        if ($files):
                if (!is_array($files)):
                        $files = array($files);
                endif;
                foreach($files as $file):
?>
                <link rel="stylesheet" href="<?php echo $file;?>" type="text/css" />
<?php
                 endforeach;
        endif;
?>
<?php //CG: overrides include: ?>
                <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/overrides.css" type="text/css" />
                <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/responsive.css" type="text/css" />

                <?php if ($this->direction == 'rtl') : ?>
                <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/template_rtl.css" type="text/css" />
                <?php endif; ?>
                <!--[if lte IE 6]>
                        <link href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/ieonly.css" rel="stylesheet" type="text/css" />
                <![endif]-->
                <!--[if IE 7]>
                        <link href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/ie7only.css" rel="stylesheet" type="text/css" />
                <![endif]-->

                <!--[if lt IE 9]>
                        <script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/javascript/html5.js"></script>
                <![endif]-->

        </head>
    <body id="body" class="site <?php print "itemId-" . $itemId?>">
    <div id="all">
        <div id="back">
                <header id="header">
                    <div class="logoheader">
                            <h1 id="logo">

                            <?php if ($logo != null ): ?>
                            <img src="<?php echo $this->baseurl ?>/<?php echo htmlspecialchars($logo); ?>" alt="<?php echo htmlspecialchars($templateparams->get('sitetitle'));?>" />
                              <?php endif; ?>
                            <span class="header1">
                            <?php echo htmlspecialchars($templateparams->get('sitedescription'));?>
                            </span></h1>
                    </div><!-- end logoheader -->
                            <?php /*<ul class="skiplinks">
                                    <li><a href="#main" class="u2"><?php echo JText::_('TPL_BEEZ5_SKIP_TO_CONTENT'); ?></a></li>
                                    <li><a href="#nav" class="u2"><?php echo JText::_('TPL_BEEZ5_JUMP_TO_NAV'); ?></a></li>
                                    <?php if($showRightColumn ):?>
                                    <li><a href="#additional" class="u2"><?php echo JText::_('TPL_BEEZ5_JUMP_TO_INFO'); ?></a></li>
                                    <?php endif; ?>
                            </ul> */ ?>
                            <?php /*<h2 class="unseen"><?php echo JText::_('TPL_BEEZ5_NAV_VIEW_SEARCH'); ?></h2>
                            <h3 class="unseen"><?php echo JText::_('TPL_BEEZ5_NAVIGATION'); ?></h3> */ ?>
                            <jdoc:include type="modules" name="position-1" />
                            <div id="line">
                            </div>
                            <?php /* <h3 class="unseen"><?php echo JText::_('TPL_BEEZ5_SEARCH'); ?></h3> */ ?>
                            <jdoc:include type="modules" name="position-0" />
                            </div> <!-- end line -->
                    <div id="header-image">
                            <jdoc:include type="modules" name="position-15" />
                            <?php if ($this->countModules('position-15')==0): ?>
                                    <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/d-town/dt-logo2013.jpg"  alt="<?php echo JText::_('TPL_BEEZ5_LOGO'); ?>" />
                            <?php endif; ?>      <?php if ($this->countModules('position-12')): ?>
                                                            <div id="top"><jdoc:include type="modules" name="position-12"   />
                                                            </div>
                                                    <?php endif; ?>
                    </div>              
                </header><!-- end header -->
                <div id="<?php echo $showRightColumn ? 'contentarea2' : 'contentarea'; ?>">
                                        <?php if ($navposition=='left' and $showleft) : ?>
                                            <nav class="left1 <?php if ($showRightColumn==NULL){ echo 'leftbigger';} ?>" id="nav">                                            
                                                <jdoc:include type="modules" name="position-7" style="beezDivision" headerLevel="3" />
                                                <jdoc:include type="modules" name="position-4" style="beezHide" headerLevel="3" state="0 " />
                                                <jdoc:include type="modules" name="position-5" style="beezTabs" headerLevel="2"  id="3" />
                                            </nav>
                                        <?php endif; ?>
                                        <?php if($this->countModules('menu')) : ?>
                                        <button onclick="toggleMenu()" type="button" id="mobileBtn" class="mobile btn btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>  
                                        <nav id="mainNav" class="mainMenu">
                                            <jdoc:include type="modules" name="menu" />
                                        </nav>
                                        <?php endif; ?>
                                        <?php if($this->countModules('breadcrumbs')) : ?>
                                            <div id="breadcrumbs">
                                                <jdoc:include type="modules" name="breadcrumbs" />
                                            </div>
                                        <?php endif; ?>
                                        <div id="<?php echo $showRightColumn ? 'wrapper' : 'wrapper2'; ?>" <?php if (isset($showno)){echo 'class="shownocolumns"';}?>>
                                                <div id="main">
                                                    <jdoc:include type="message" />
                                                    <jdoc:include type="component" />
                                                </div><!-- end main -->
                                        </div><!-- end wrapper -->

                                <?php if ($showRightColumn) : ?>
                                        <h2 class="unseen">
                                                <?php echo JText::_('TPL_BEEZ5_ADDITIONAL_INFORMATION'); ?>
                                        </h2>
                                        <div id="close">
                                                <a href="#" onclick="auf('right')">
                                                        <span id="bild">
                                                                <?php echo JText::_('TPL_BEEZ5_TEXTRIGHTCLOSE'); ?></span></a>
                                        </div>
                                        <aside id="right">
                                            <a id="additional"></a>
                                            <jdoc:include type="modules" name="position-6" style="beezDivision" headerLevel="3"/>
                                            <jdoc:include type="modules" name="position-8" style="beezDivision" headerLevel="3"  />
                                            <jdoc:include type="modules" name="position-3" style="beezDivision" headerLevel="3"  />
                                        </aside>
                        <?php endif; ?>

                        <?php if ($navposition=='center' and $showleft) : ?>
                            <nav class="left <?php if ($showRightColumn==NULL){ echo 'leftbigger';} ?>" id="nav">
                                <jdoc:include type="modules" name="position-7"  style="beezDivision" headerLevel="3" />
                                <jdoc:include type="modules" name="position-4" style="beezHide" headerLevel="3" state="0 " />
                                <jdoc:include type="modules" name="position-5" style="beezTabs" headerLevel="2"  id="3" />
                            </nav>
                        <?php endif; ?>
                    <div class="wrap"></div>
                </div> <!-- end contentarea -->
        </div><!-- back -->
    </div><!-- all -->
        <div id="footer-outer">
        <?php if ($showbottom) : ?>
                <div id="footer-inner">

                        <div id="bottom">
                                <?php if ($this->countModules('position-9')): ?>
                                <div class="box box1"> <jdoc:include type="modules" name="position-9" style="beezDivision" headerlevel="3" /></div>
                                <?php endif; ?>
                                   <?php if ($this->countModules('position-10')): ?>
                                <div class="box box2"> <jdoc:include type="modules" name="position-10" style="beezDivision" headerlevel="3" /></div>
                                <?php endif; ?>
                                <?php if ($this->countModules('position-11')): ?>
                                <div class="box box3"> <jdoc:include type="modules" name="position-11" style="beezDivision" headerlevel="3" /></div>
                                <?php endif ; ?>
                        </div>
                </div>
        <?php endif ; ?>

                <div id="footer-sub">
                    <footer id="footer">
                        <jdoc:include type="modules" name="position-14" />
                        <p>
                            <a class="footerLink" href="/impressum">Impressum</a> | <a class="footerLink" href="/datenschutzerklaerung">Datenschutzerkl&auml;rung</a>
                        <p>
                            <p>
                                &copy; <?php echo date("Y");?> Downtown Studios München - Tonstudio Augustenstr. 60 D-80333 München Tel. +49-089-527142
                            </p>
                        </p>
                    </footer>
                </div>
        </div>
        <jdoc:include type="modules" name="debug" />
        <script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/javascript/hide.js"></script>

        <script>
/*                var big ='<?php echo (int)$this->params->get('wrapperLarge');?>%';
                var small='<?php echo (int)$this->params->get('wrapperSmall'); ?>%';
                var altopen='<?php echo JText::_('TPL_BEEZ5_ALTOPEN', true); ?>';
                var altclose='<?php echo JText::_('TPL_BEEZ5_ALTCLOSE', true); ?>';
                var bildauf='<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/plus.png';
                var bildzu='<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/minus.png';
                var rightopen='<?php echo JText::_('TPL_BEEZ5_TEXTRIGHTOPEN', true); ?>';
                var rightclose='<?php echo JText::_('TPL_BEEZ5_TEXTRIGHTCLOSE', true); ?>';
*/

                <?php //CG: basic mobile detection für mobile Menu und button-toggle: ?>

                var body = document.getElementById('body');

                window.addEventListener('resize', function(){

                    var width = window.innerWidth;
                        if(width < 1366 && !isMobileDevice()) {
                            body.classList.add('resize');
                        } else {
                            body.classList.remove('resize');
                        }
                });
                    if(isMobileDevice()) {
                        body.classList.add('mobile');
                    } else {
                        body.classList.remove('mobile');
                    }                        
                    
                    if (isMobileDevice()) { window.onload = toggleMenu(); }
                    function isMobileDevice() {
                        return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
                    };
                    function toggleMenu() {
                        var button = document.getElementById("mobileBtn");
                        var menu = document.getElementById("mainNav");
                        if (menu.style.display === "none") {
                            menu.style.display = "block";
                                button.classList.add("open");
                                menu.classList.add("animate");
                        } else {
                            menu.style.display = "none";
                                button.classList.remove("open");
                                menu.classList.remove("animate");
                        }
                    }

        </script>                
    </body>
</html>
