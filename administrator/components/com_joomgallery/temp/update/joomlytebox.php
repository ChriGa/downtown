<?php
// $HeadURL: https://joomgallery.org/svn/joomgallery/JG-2.0/Plugins/JoomLytebox/trunk/joomlytebox.php $
// $Id: joomlytebox.php 4193 2013-04-12 18:10:21Z erftralle $
/******************************************************************************\
**   JoomGallery Plugin 'Integrate Lytebox'                                   **
**   By: JoomGallery::ProjectTeam                                             **
**   Copyright (C) 2012 JoomGallery::ProjectTeam                              **
**   Released under GNU GPL Public License                                    **
**   License: http://www.gnu.org/copyleft/gpl.html                            **
\******************************************************************************/

// No direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.plugin.plugin');

/**
 * JoomGallery Plugin 'Integrate Lytebox'
 *
 * With this plugin JoomGallery is able to use the Lytebox Javascript library
 * (http://lytebox.com/) for displaying images.
 *
 * NOTE: Please remember that Lytebox is licensed under the terms of the
 * 'Creative Commons Attribution 3.0 License':
 *
 * http://lytebox.com/license.php
 * http://creativecommons.org/licenses/by/3.0/
 *
 * @package     JoomGallery
 * @since       2.0
 */
class plgJoomGalleryJoomLytebox extends JPlugin
{
  /**
   * Name of this popup box
   *
   * @var   string
   * @since 2.0.1
   */
  protected $title = 'Lytebox';

  /**
   * Joomgallery configuration
   *
   * @var     object
   */
  private $_jg_config = null;

  /**
   * Constructor
   *
   * @param   object  $subject  The object to observe
   * @param   array   $config   An array that holds the plugin configuration
   * @return  void
   * @since   2.0
   */
  public function __construct(& $subject, $config)
  {
    parent::__construct($subject, $config);
    $this->loadLanguage();
  }

  /**
   * Method to handle the 'onJoomOpenImage' event
   *
   * @param   string  $link     The link to modify
   * @param   object  $image    An object holding the image data
   * @param   string  $img_url  The URL to the image which shall be openend
   * @param   string  $group    The name of an image group, RokBox will make an album out of the images of a group
   * @param   string  $type     'orig' for original image, 'img' for detail image or 'thumb' for thumbnail
   * @return  void
   * @since   2.0
   */
  public function onJoomOpenImage(&$link, $image = null, $img_url = null, $group = 'joomgallery', $type = 'orig')
  {
    static $loaded = false;

    if($image)
    {
      if(!$loaded)
      {
        JHtml::_('behavior.framework');

        $doc = JFactory::getDocument();

        // Add variables to forward color theme and some translations to Lytebox
        $script  = "    var lyteboxTheme         = '".$this->params->get('cfg_theme')."';\n";
        $script .= "    var lyteboxCloseLabel    = '".JText::_('PLG_JOOMGALLERY_JOOMLYTEBOX_CLOSE_LBL')."';\n";
        $script .= "    var lyteboxPrevLabel     = '".JText::_('PLG_JOOMGALLERY_JOOMLYTEBOX_PREV_LBL')."';\n";
        $script .= "    var lyteboxNextLabel     = '".JText::_('PLG_JOOMGALLERY_JOOMLYTEBOX_NEXT_LBL')."';\n";
        $script .= "    var lyteboxPlayLabel     = '".JText::_('PLG_JOOMGALLERY_JOOMLYTEBOX_PLAY_LBL')."';\n";
        $script .= "    var lyteboxPauseLabel    = '".JText::_('PLG_JOOMGALLERY_JOOMLYTEBOX_PAUSE_LBL')."';\n";
        $script .= "    var lyteboxPrintLabel    = '".JText::_('PLG_JOOMGALLERY_JOOMLYTEBOX_PRINT_LBL')."';\n";
        $script .= "    var lyteboxImageLabel    = '".JText::_('PLG_JOOMGALLERY_JOOMLYTEBOX_IMAGE_LBL')."';\n";
        $script .= "    var lyteboxPageLabel     = '".JText::_('PLG_JOOMGALLERY_JOOMLYTEBOX_PAGE_LBL')."';\n";
        $script .= "    window.addEvent('domready', function() {\n";
        $script .= "      var sstr = '".strtolower($this->title)."';\n";
        $script .= "      $$('a[rel^=' + sstr + ']').each(function(el) {\n";
        $script .= "        el.addClass(sstr);\n";
        $script .= "      });\n";
        $script .= "    });";

        $doc->addScriptDeclaration($script);

        // Add lytebox css and js
        $doc->addStyleSheet(JURI::root().'media/plg_joomgallery_joomlytebox/lytebox.css');
        $doc->addScript(JURI::root().'media/plg_joomgallery_joomlytebox/lytebox.js');

        // Get JoomGallery configuration
        $this->_jg_config = JoomConfig::getInstance();

        $loaded = true;
      }

      // Prepare the data title used in lytebox
      $data_title = '';
      if($this->_jg_config->get('jg_show_title_in_popup'))
      {
        $data_title .= $image->imgtitle;
      }
      if($this->_jg_config->get('jg_show_description_in_popup') && !empty($image->imgtext))
      {
        if($this->_jg_config->get('jg_show_description_in_popup') == 1)
        {
          $data_title .= htmlspecialchars($image->imgtext);
        }
        else
        {
          if(!empty($data_title))
          {
            $data_title .= '<br />';
          }
          $data_title .= htmlspecialchars(strip_tags($image->imgtext));
        }
      }

      // Prepare some lytebox options
      $autoResize   = $this->_jg_config->get('jg_resize_js_image') ? 'true' : 'false';
      $resizeSpeed  = $this->params->get('cfg_resizespeed', 5);
      $navType      = $this->params->get('cfg_navtype');
      $navTop       = $this->params->get('cfg_navbarpos') ? 'false' : 'true';
      $titleTop     = $this->params->get('cfg_titlepos') ? 'false' : 'true';

      // Prepare the thumbnail link dependent on lytebox operation mode
      switch((int)$this->params->get('cfg_operationmode'))
      {
        case 1:
          // Prepare some extra slideshow options
          $loopSlideshow = $this->params->get('cfg_slideloop') ? 'true':'false';
          $autoEnd = 'false';
          if($loopSlideshow === 'false')
          {
            $autoEnd = 'true';
          }
          // Slideshow
          $link = $img_url.'" rel="'.strtolower($this->title).'" data-lyte-options="slide:true'
                                                                     .' loopSlideshow:'.$loopSlideshow
                                                                     .' autoEnd:'.$autoEnd
                                                                     .' autoResize:'.$autoResize
                                                                     .' resizeSpeed:'.$resizeSpeed
                                                                     .' navType:'.$navType
                                                                     .' navTop:'.$navTop
                                                                     .' titleTop:'.$titleTop
                                                                     .' group:'.$group
                                                                     .' slideInterval:'.(int)$this->params->get('cfg_slideinterval')
                                                                     .'" data-title="'.$data_title;
          break;
        default:
          // Grouped popup box
          $link = $img_url.'" rel="'.strtolower($this->title).'" data-lyte-options=" autoResize:'.$autoResize
                                                                      .' resizeSpeed:'.$resizeSpeed
                                                                      .' navType:'.$navType
                                                                      .' navTop:'.$navTop
                                                                      .' titleTop:'.$titleTop
                                                                      .' group:'.$group
                                                                      .'" data-title="'.$data_title;
          break;
      }
    }
    else
    {
      // JoomGallery wants to know whether this plugin is enabled
      $link = true;
    }
  }
}